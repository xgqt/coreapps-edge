# CoreApps Master repo
This contains all the CoreApps repo in latest git commit.

### 1.Download
To download CoreApps suite, open a terminal and type:

	$ git clone https://gitlab.com/cubocore/coreapps-edge
	$ cd coreapps-edge
	$ ./update-all

### 2.Compilation
First install all the dependencies using this, Use correct one according to your distro

	$ ./debian-depend
	or
	$ ./arch-depend

To compile and install all the repos:

	$ mkdir build && cd build
	$ cmake -DCMAKE_INSTALL_PREFIX=/usr ..
	$ make -kj$(nproc) && sudo make install

In case you do not want to install the coreapps, you can simply skip the ` && sudo make install` in the last command above.

### Depedencies
* qt5-base
* qt5-x11extras
* qt5-multimedia
* qt5-svg
* qt5-connectivity
* qt5-serialport
* cryfs
* libxrender
* libxcomposite
* libx11
* libxdamage
* libzen
* libmediainfo
* libsensors4-dev
* libxtst
* libx11
* libArchive(libarchive-dev)
* libLzma(xz-dev)
* libBz2(bzip2-dev)
* zlib
* liblz (lzip)
* lzop (binary)
* xcb-util
* xcb-damage

### Information
Please see the [Wiki page](https://gitlab.com/cubocore/wiki) for this info.
* Changes in latest release ([ChangeLog](https://gitlab.com/cubocore/wiki/-/blob/master/ChangeLog))
* Build from the source ([BuildInfo](https://gitlab.com/cubocore/wiki/blob/master/BuildInfo.md))
* Tested In ([Test System](https://gitlab.com/cubocore/wiki/blob/master/TestSystem))
* Known Bugs ([Current list of issues](https://gitlab.com/groups/cubocore/coreapps/-/issues))
* Help Us

### Feedback
* We need your feedback to improve the C Suite. Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/coreapps/-/issues).
  
  Or feel free to join and chat with us in IRC/Matrix #cubocore:matrix.org or [Element.io](https://app.element.io/#/room/#cubocore:matrix.org)
