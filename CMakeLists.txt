project( CoreApps )
cmake_minimum_required( VERSION 3.8 )

add_definitions ( -Wall )
if ( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT )
    set( CMAKE_INSTALL_PREFIX "/usr" CACHE PATH "Location for installing the project" FORCE )
endif()

message( "Installing to ${CMAKE_INSTALL_PREFIX}" )

add_subdirectory( libcprime )
add_subdirectory( libcsys )
add_subdirectory( libarchive-qt )

include_directories(
    libcprime
    libcsys
    libcprime/cprime
    libcsys/csys
    libarchive-qt/lib
)

link_directories( ${CMAKE_CURRENT_BINARY_DIR}/libcprime/ )
link_directories( ${CMAKE_CURRENT_BINARY_DIR}/libcsys/ )
link_directories( ${CMAKE_CURRENT_BINARY_DIR}/libarchive-qt/lib/ )

add_subdirectory( coreaction )
add_subdirectory( corearchiver )
add_subdirectory( corefm )
add_subdirectory( coregarage )
add_subdirectory( corehunt )
add_subdirectory( coreimage )
add_subdirectory( coreinfo )
add_subdirectory( corekeyboard )
add_subdirectory( corepad )
add_subdirectory( corepaint )
add_subdirectory( corepdf )
add_subdirectory( corepins )
add_subdirectory( corerenamer )
add_subdirectory( coreshot )
add_subdirectory( corestats )
add_subdirectory( corestuff )
add_subdirectory( coreterminal )
add_subdirectory( coretime )
add_subdirectory( coretoppings )
add_subdirectory( coreuniverse )

add_dependencies( coreaction cprime-widgets csys )
add_dependencies( corearchiver cprime-widgets csys )
add_dependencies( corefm cprime-widgets csys )
add_dependencies( coregarage cprime-widgets csys )
add_dependencies( corehunt cprime-widgets csys )
add_dependencies( coreimage cprime-widgets csys )
add_dependencies( coreinfo cprime-widgets csys )
add_dependencies( corekeyboard cprime-widgets csys )
add_dependencies( corepad cprime-widgets csys )
add_dependencies( corepaint cprime-widgets csys )
add_dependencies( corepdf cprime-widgets csys )
add_dependencies( corepins cprime-widgets csys )
add_dependencies( corerenamer cprime-widgets csys )
add_dependencies( coreshot cprime-widgets csys )
add_dependencies( corestats cprime-widgets csys )
add_dependencies( corestuff cprime-widgets csys )
add_dependencies( coreterminal cprime-widgets csys )
add_dependencies( coretime cprime-widgets csys )
add_dependencies( corepkit cprime-widgets csys )
add_dependencies( coreuniverse cprime-widgets csys )
